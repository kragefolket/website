# Kragefolket website

> This project has been moved to https://dev.azure.com/kragefolket/website to support Office 365 logins

Urls:

* [Admin](https://www.kragefolket.dk/admin)
* [Public](https://www.kragefolket.dk/)

We also have

* [Preview](https://preview.kragefolket.dk/) for non master branches (testing) ([admin](https://kragefolketpreview.balle-net.dk/admin/))
