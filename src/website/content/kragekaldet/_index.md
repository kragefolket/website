---
title: Kragekaldet
windowtitle: Kragekaldet, Medlemsbladet for KFUM-Spejderne i Hellerup
menu:
  main:
    weight: 50
sitemap:
  changefreq: monthly
  priority: 0.5
---

# Kragekaldet

Kragekaldet er vores gruppeblad. Nu til dags bliver det udgivet som en pdf der bliver sendt til alle medlemmer. Her kan du finde nye og gamle versioner af Kragekaldet.
