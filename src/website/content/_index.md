---
title: Forside
windowtitle: Kragefolket, KFUM-Spejderne i Hellerup
features:
  - title: Kragekaldet
    description: >-
      Vores blad der udkommer fire gange om året.

      Her kan du læse nyt fra enhederne, finde program og se billeder fra vores oplevelser.
    icon: Person med kamera
    image: /media/hjemmeside.jpg
    url: /kragekaldet
    weight: "20"
  - title: Billeder
    description: Vi tager lidt billeder fra vores møder og ture. De bedste kommer i
      Kragekaldet, men du kan se flere i vores billedarkiv
    icon: Person med kamera
    image: /media/kamera.jpg
    url: /billeder
    weight: "50"
  - title: Spejder?
    description: Spejderarbejde er værdibaseret, vi er mere end et godt fritidstilbud.
    icon: Seje spejdere
    image: /media/hjemmeside2.jpg
    url: /bliv-spejder
    weight: "10"
footeritems:
  - leftimage: /media/logoer/kfum_rgb_vert-160.png
    rightimage: /media/logoer/krage1-160.png
    text: "# Hellerup Gruppe – Kragefolket\r


      Hellerup Gruppe er en af de ældste grupper hos KFUM-Spejderne i
      Danmark og har eksisteret siden 1919. Hellerup Gruppe er tilknyttet det
      kirkelige ungdomsarbejde i Hellerup sogn og Hellerup kirke. Vi holder til
      i Ungdomshuset på Margrethevej 9, 2900 Hellerup."
description: Vi er en aktiv spejdergruppe med cirka 130 medlemmer, og du er også
  velkommen. Med opstart i 1919 er vi en af de ældste grupper hos KFUM-Spejderne
sitemap:
  changefreq: monthly
  priority: 0.6
---
