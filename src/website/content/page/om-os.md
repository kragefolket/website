---
title: Om os
url: /om-os
windowtitle: Om Kragefolket, KFUM-Spejderne i Hellerup
description: KFUM-Spejderne i Hellerup er fra 1919 og dermed en af landets
  ældste. Læs kort om gruppen, vores faciliteter og kontaktoplysninger.
sitemap:
  changefreq: monthly
  priority: 0.7
publishdate: ""
menu:
  main:
    weight: 80
---
# Om os

Kort om KFUM-Spejderne i Hellerup, Kragefolket

* En af de 15 ældste grupper hos KFUM-Spejderne i Danmark, startet i 1909
* Har ca. 130 medlemmer
* Mødes primært i Ungdomshuset overfor Hellerup Kirke
* Har egne hytter i Nordsjælland, kanocenter og andel i en større sejlbåd

Vi holder til i Ungdomshuset overfor Hellerup Kirke:

KFUM-Spejderne i Hellerup, Kragefolket\
Margrethevej 9\
2900 Hellerup

## Kontakt os

Send os en mail på [mail@kragefolket.dk](mailto:mail@kragefolket.dk)

Kontaktpersoner:

* Gruppeleder: Bjørn Jessen, tlf. 4074 1779, og Jesper Balle, tlf. 2683 7131 [gruppeleder@kragefolket.dk ](mailto:gruppeleder@kragefolket.dk)
* Gruppebestyrelsesformand: Vibeke Würtz Jürgensen, tlf. 6130 0573, [formand@kragefolket.dk](mailto:formand@kragefolket.dk)
* Kasserer: Marie Nouard, tlf. 2623 8580, [kasserer@kragefolket.dk](mailto:kasserer@kragefolket.dk)

Kontakt vedrørende møder og arrangementer i de enkelte enheder til lederen [se de enkelte enheder](/bliv-spejder)

## Ungdomshuset

Ungdomshuset er til det frivillige ungdomskirkelige arbejde i Hellerup og således er der i stue-etagen lokaler til pt. KFUM-Spejderne. Udover de fælles opholdsarealer råder gruppen over sit egne lokaler, hvor spejderånden kan regere. Desuden har vi kælderfaciliteter, der huser gruppens store samling af materiel.

På 1. salen er to lejligheder, hvor det ene er med 3 værelser og fælles køkken. Alle beboere i Ungdomshuset er aktive ledere i de to foreninger og der søges en nogenlunde lige fordeling.

## Kort historie

Kragefolket er en de 15 ældste grupper hos KFUM-Spejderne i Danmark. Gruppen startede som FDF-kreds, men blev i 1919 optaget i korpset som Hellerup Division (senere Gentofte Kommunes Division) med hele tre troppe.

I dag er vi en velfungerende gruppe med et medlemstal på ca. 130. Vi har seks gode børne- og ungeenheder, hvor det daglige arbejde bygger på spejderbevægelsens grundlæggende principper om idé- og holdningsbetonet kristent børnearbejde. Vi forsøger at gøre vores børn og unge til selvstændige ansvarlige individer, der tager vare på sig selv og sine medmennesker, således at de kan klare sig selv og hjælpe andre.

Kontinuitet og inspirerende børne- og ungearbejde er nøgleord i såvel enhedernes som gruppens planlægning. Derfor har vi et klart mål, at der altid skal være tre ledere af blandet køn i alle enheder. Vi forsøger løbende at uddanne os, og mange ledere søger inspiration hos andre ledere og/eller unge på korps- eller distriktsplan.

De daglige møder gennemføres primært i Hellerup Ungdomshus, overfor Hellerup Kirke. Derudover råder gruppen over egne patrulje- og trophytter i Nordsjælland samt eget kanocenter og andel i en større sejlbåd.

## Faciliteter

**Blokhuset** er vores weekendhytte nær Værløse, den kan huse en hel trop (28 sengepladser), og vi lejer den også ud til andre spejdere. Læs mere om Blokhuset i Hyttefortegnelsen.

**Kragebo** er en mindre patruljehytte nær Allerød.

**Kanocenter** har vi også. Seks kanoer på tilhørende kanotrailer og veste i alle størrelser. Vi lejer også kanoerne ud til andre spejdere eller forældre, som gerne vil ud og sejle i kano. Kontakt Jess Simonsen, tlf. 4095 5840

**Lederbolig** har vi mulighed for at tilbyde. Ungdomshuset blev bygget i 1985 af Kirkekredsen, og der er med hjælp fra lokal fond etableret lederboliger på 1. salen til unge, som er aktive i det ungdomskirkelige arbejde i sognet. Der er en treværelses lejlighed og tre klubværelser. Se mere om [leder bolig](/leder-bolig).

Ansøgning sendes til Bjørn Jessen, tlf. 3965 0868, lyngbyvejen(a)email.dk
