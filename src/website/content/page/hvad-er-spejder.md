---
title: Spejder?
url: /hvad-er-spejder
sitemap:
  changefreq: monthly
  priority: 0.1
publishdate: 2019-06-15T12:12:03.890Z
menu:
  main:
    weight: 10
---
# Spejder?

At være spejder indebærer mange ting. Der er plads til udfordrende oplevelser, hvor man bliver presset til det yderste, men også til sjove og hyggelige øjeblikke sammen med gode venner. Når spejderne løser en opgave sammen, lærer de at respektere hinanden og at tage ansvar. Gennem spejderarbejdet lærer alle spejdere at finde deres egen mening.

Hellerup gruppe er en del af KFUM-Spejderne i Danmark, hvis mål er at skabe hele mennesker. Det gør vi ved at udvikle spejderne igennem fem områder: Fysisk, Intellektuelt, Følelsesmæssigt, Socialt og Åndeligt. Det sker på de ugentlige møder for forskellige aldersgruppper, hvor frivillige ledere planlægger og afvikler et fremadskridende program baseret på spejdermetoden.

For mere information om [KFUM-Spejderne og spejdermetoden](https://kfumspejderne.dk/bliv-spejder/spejdermetoden/).
