---
title: Er du vores nye leder - lige nu kan vi tilbyde bolig
url: /leder-bolig
thumbnail: /media/kollegiet.jpg
sitemap:
  changefreq: monthly
  priority: 0.5
publishdate: 2022-01-15T20:58:04.546Z
images:
  - format: landscape
    moid: 15310
    image: /media/kollegiet.jpg
  - format: landscape
    moid: 15311
    caption: Fælleskøkken for klubværelser
    image: /media/uh/uh-koekken.jpg
  - format: landscape
    caption: Klubværelse bad
    moid: 15307
    image: /media/uh/klublokale-bad.jpg
  - format: landscape
    image: /media/uh/klublokale-rum1.jpg
    caption: Klubværelse
    moid: 15308
  - format: landscape
    moid: 15309
    image: /media/uh/klublokale-rum2.jpg
    caption: Klubværelse
---

# Brænder du for spejderarbejdet og mangler du et sted at bo?

Vi kan sammen med Hellerup Kirkekreds lige nu tilbyde et værelse til en leder i vores "Spejderkollegie". Vi kunne godt bruge en leder i troppen, men det kunne også blive en anden enhed. Det vigtigste er at du har lyst til at være ugentlig leder.

Du er:

* Ung
* Brænder for at være spejderleder
* Dygtig leder
* Klar på sjov

Vi tilbyder:

* stærke lederteams i alle enheder fra bævere til seniorer
* de skønneste spejderunger
* en gruppe med over 100 medlemmer
* mulighed for at byde ind med de sjoveste idéer og se dem blive til virkelighed
* sociale og faglige arrangementer for lederne

## Hvordan kan man komme i betragtning

Skriv en lille ansøgning om dig selv og hvorfor du er den rette og send den til formand for Hellerup Kirkekreds: Bjørn Jessen, [lyngbyvejen@email.dk](mailto:lyngbyvejen@email.dk), tlf. 4074 1779.

Husudvalget (som pt. består af formanden og to repræsentanter fra spejdergruppen) vil derefter aftale tidspunkt for en samtale. Indflytning efter aftale (det er ledigt fra 1. februar)

## Om kollegiet

Ungdomshuset er et lille kollegie, hvor tre beboere deler køkken, men hver især har et værelse med tilhørende badeværelse.
En del af kollegiet er også en lejlighed, hvor der bor et par.

Vi er sociale, nyder at spise aftensmad i ny og næ og holder sociale aktiviteter. Så det er vigtigt, at du synes, det er nice at være en del af fællesskabet.

## Om værelset

Værelset er 15 kvm, plus eget toilet og bad samt egen entre med mulighed for at bruge haven. Huset er i gåafstand til Hellerup stationen og strand. Huslejen ligger på 1850 kr. om måneden med forbrug inkluderet. I huset er der ydermere en vaskemaskine og tørretumbler til rådighed for 125 kr. om måneden. 

## Om stedet

Ungdomshuset i Hellerup ejes af foreningen Hellerup Kirkekreds og er bygget til børne-/ungdomsarbejde i Hellerup Sogn. I Hellerup er der både S-tog og Kystbanen, så der er cirka 15 min til Nørreport st. med offentlig transport og 20 min. på cykel.

I stueetagen har spejdergruppen lokaler og øverst er bolig for 5 personer. Da huset blev bygget i 1985 indgik lokale fondsmidler i byggeprojektet med den betingelse, at der skulle være billige boliger for aktive ledere i sognet. Pt. er der kun KFUM-Spejdere, men der har tidligere også været KFUM&K, Børnekonfirmand ansvarlig o.lign.

## Om gruppen

Vi har skrevet lidt kort [om Hellerup Gruppe](/om-os) under om os. Vi har ledere i alle aldre og har generelt en god lederbesætning, således har alle enheder min. 3 ledere (så man kan melde afbud ved sygdom, eksamen osv. uden at skulle aflyse). Vi holder stabsmøde for alle ledere cirka 5 gange om året, hvor vi starter med fælles aftensmad.

Alle enheder holder møde hver uge og typisk et par weekendture. Derudover har vi nogle faste gruppearrangementer, såsom opstartsmøde i august, juleweekend den første weekend i december, fastelavn, halloween.
