---
title: Bliv spejder
url: /bliv-spejder
windowtitle: Bliv spejder i Hellerup - en aktiv fritidsaktivitet for børn og unge
description: >-
  Spejder er for alle og man kan løbende starte hos os, se hvornår vi holder
  spejdermøde hvor I kan være med.
sitemap:
  changefreq: monthly
  priority: 0.7
publishdate: ''
menu:
  main:
    title: Vær med
    weight: 20
images: []
---
# Vær med

Spejderarbejdet er åben for alle, se nedenfor hvornår de enkelte aldersgrene (vi kalder dem enheder) afholder deres faste spejdermøder. Man kan løbende starte, tag gerne kontakt til den ansvarlige for aldersgruppen og kom og prøv et par gange.

{{< enhederne >}}
