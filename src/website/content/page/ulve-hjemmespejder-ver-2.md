---
title: Ulve Hjemmespejder ver. 2
url: /ulve-hjemmespejd
windowtitle: Hjemmespejd ver. 2 - Ulve - Kragefolket Hellerup
description: Hjemmespejder opgaverne
sitemap:
  changefreq: never
  priority: 0.5
publishdate: 2021-02-11T18:29:52.414Z
images: []
---
# Hjemmespejd version 2 - Ulvene

Så er vi her igen!

Ligesom sidste forår er vi næsten spærret inde derhjemme, og må hverken komme i skole eller til ulvemøder.... ØV!

Men som spejdere og især ulve giver vi naturligvis IKKE op!!!

Vi må bare lave spejderarbejdet derhjemme. Derfor har vi igen i år lavet et HJEMMESPEJD-mærke til uniformen, som man kan gøre sig fortjent til ved at løse 7 af de 12-14 opgaver, vi vil præsentere for jer i løbet af februar, eller hvor længe det nu kommer til at tage, før vi kan holde ulvemøder igen.

![](/media/ulve-hjemmespejd-ver2-maerke.jpg)

Opgaverne vil komme til at ligge her på siden og i [Facebook-gruppen](https://www.facebook.com/groups/1082655288902883), der er en gruppe KUN for ulvene og deres forældre og så selvfølgelig jeres seje ulveledere 😉 (så I er nok nødt til at bruge jeres forældres Facebook-profiler)

Man kan sende sine løsninger ved at lave kommentarer til de enkelte opgaver i [Facebook gruppen](https://www.facebook.com/groups/1082655288902883) eller ved at sende dem i en mail til [ulve@kragefolket.dk](mailto:ulve@kragefolket.dk) Hvis I gør det på facebook, må I lige skrive, hvilken ulv der svarer, for ellers vil det se ud som om det er jeres forældre, der svarer.

Til nogle af opgaverne vil vi prøve at mødes på nettet på forskellig måde. Det sker om torsdagen til normal mødetid, men det kommer nok ikke til at vare ligeså længe som et almindeligt møde. Til gengæld får vi set hinanden og kan følge med i, hvordan det går.

Med hylende ulvehilsen,\
Lederne

## Opgaverne

1. Woop løb (sendt fra seniorer til alle i Hellerup Gruppe)
2. Genkend dyrene
3. Online ulvemøde
4. Opfinde fagter til vores nye junglestartsang

### Opgave 2 Genkend dyrene

> [Download svar ark med for alle dyrene](http://www.crowwwebdesign.dk/mixdyr.pdf)

Så er vi klar med den første opgave.
Det viser sig, at Corona desværre også har ramt dyreverdenen og har medført, at der er kommet nye dyreracer til, som ser ud til at være blandinger af forskellige andre racer. Se blot billederne nedenfor:
Vores super fotograf har været verden rundt og taget billeder af de nye dyreracer, og I kan se alle billederne, hvis I går ind på http://www.crowwwebdesign.dk/mixdyr.pdf .
Og jeres opgave er at finde ud af hvilke andre dyreracer de tyve nye racer er blandinger af.  Send jeres løsning til ulve@kragefolket.dk 
Hvis I får brug for hjælp, kan i prøve at gå ind på youtube.com og søge på den video, der hedder Hybrid Birds.  Den er også et kig værd, selv om du godt kan løse opgaven uden. For den er ret sjov.

### Opgave 3 Online ulvemøde

Vi prøver at lave lidt spejderaktiviter online torsdag kl. 19.00
[Start online møde](https://teams.microsoft.com/l/meetup-join/19%3ameeting_MWRlNzM4NTQtYTdiNi00MmZlLThmZDAtZDc0ODNlMDYwMjQ5%40thread.v2/0?context=%7b%22Tid%22%3a%22b7017121-1166-4b0c-af6a-ad6a4acc02ca%22%2c%22Oid%22%3a%228d4c12eb-9f05-4f48-9e29-06be66449cf6%22%7d) (den 11. og 18. februar)

Den 25. februar skal vi på et online løb. Man skal helst bruge en computer (fremfor telefon/tablet) med kamera og mikrofon. Vi har lavet [en lille gennemgang her](https://youtu.be/072TjqxhObk) som man med fordel kan se inden. [Start det online spejderløb her](https://gather.town/app/6hRdm5HTEiLLFUkm/UH-fv).

### Opgave 4 Opfinde fagter til vores nye junglestartsang

Her gælder det om at finde på nogle tydelige fagter, som understreger de vigtige ord i sangen, så den bliver nemmere at huske. 

> [Hør melodien (med jungletrommer, så du er sikker på, at du holder den rette junglerytme)](http://www.crowwwebdesign.dk/junglestartsang2.mp3)

**Jungle-møde-startsang** \
*tekst og musik: Steffen Brysting*

```
       (Rytmeklap)
Leder: Hør nu her!
Ulve:  .... hvor junglen kalder
leder: Så ' vi klar!
Ulve:  ... til dens rabalder
Alle:  Når vi mødes her i junglen i grøn kulør
       skal vi løbe, lytte lege med højt humør
       Vil du ikk' med os
       må du være skør
Leder: Hver en ulv..
Ulve:  ... følger den gamle ..... ulv
Leder: Hvis den vil...
Ulve:  lidt visdom samle
Leder: og når vi problemer møder, gi'r vi ej op
       for blandt junglekammerater er hjælpen i top
       med lidt spejderånd
       når vi helt derop
```