var path = require('path');
const ExtractCssChunks = require("extract-css-chunks-webpack-plugin")
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = function(webpack) {

    const devMode = process.env.NODE_ENV === 'development'

    var result = {
        entry: {
            main: './index.js',
            gallery: './gallery.js'
        },
        output: {
            filename: '[name].js',
            path: path.resolve(__dirname, '../assets')
        },
        devtool: devMode ? 'source-map' : 'cheap-source-map',

        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: [
                        ExtractCssChunks.loader,
                        { loader: 'css-loader' },
                        { 
                            loader: 'postcss-loader',
                            options: {
                            plugins: function () {
                                    return [
                                        require('autoprefixer')
                                    ];
                                }
                            }
                        },
                        { loader: 'sass-loader' },
                    ]
                }
            ]
        },

        externals: {
            jquery: '$'
        },

        plugins: [
            new ExtractCssChunks(
                {
                  // Options similar to the same options in webpackOptions.output
                  // both options are optional
                  filename: "[name].css",
                  chunkFilename: "[id].css",
                  hot: true, // if you want HMR - we try to automatically inject hot reloading but if it's not working, add it to the config
                  orderWarning: true, // Disable to remove warnings about conflicting order between imports
                  reloadAll: true, // when desperation kicks in - this is a brute force HMR flag
                  cssModules: true // if you use cssModules, this can help.
                }
            )
        ]
    };

    if(devMode) {
        result.devServer = {
            port: 9000
        };
    } else {
        result.plugins.push(
            new OptimizeCssAssetsPlugin({
                cssProcessorPluginOptions: {
                  preset: ['default', { discardComments: { removeAll: true } }],
                }
            })
        );
    }

    return result;
};