# How it created

## Steps performed to create the website

```sh
# Create website
docker run --rm -v ${PWD}:/repo jguyomard/hugo-builder:latest hugo new site /repo/src/website
# Add theme
git submodule add https://github.com/devcows/hugo-universal-theme src/themes/hugo-universal-theme

# Generate files
docker run --rm -v ${PWD}:/repo jojomi/hugo:latest hugo --destination /repo/output --source /repo/src/website --config src/website/config --buildDrafts --templateMetricsHints --templateMetrics --log --verbose

# Run in local serve mode
docker run --rm --name hugo -v ${PWD}:/repo -p 3000:3000 --workdir /repo -d jojomi/hugo:latest hugo server --source /repo/src/website --config src/website/config --buildDrafts --templateMetricsHints --templateMetrics --port 3000 --watch --bind=0.0.0.0 --disableFastRender --log --verbose
```
